package com.alp.academy.main.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alp.academy.main.model.UserBean;
import com.alp.academy.main.repositories.RegisterRepository;

@Service
public class RegisterServiceImpl implements RegisterService{

	@Autowired
	private RegisterRepository registerRepository;
	
	@Override
	public void addData(UserBean user) {
		registerRepository.save(user);
	}

	@Override
	public List<UserBean> getByLs(String ls, String mes) {
		List<UserBean> unfilteredList = registerRepository.getByLs(ls, mes);
		List<UserBean> filteredList = new ArrayList<>();
		for (UserBean user : unfilteredList) {
			if (user.getDate().contains("-" + mes + "-")) {
				filteredList.add(user);
			}
		}
		return filteredList;
	}

	@Override
	public List<UserBean> getRegisterByMonth(String mes) {
		List<UserBean> unfilteredList = registerRepository.getRegisterByMonth(mes);
		List<UserBean> filteredList = new ArrayList<>();
		for (UserBean user : unfilteredList) {
			if (user.getDate().contains("-" + mes + "-")) {
				filteredList.add(user);
			}
		}
		return filteredList;
	}

	@Override
	public List<UserBean> getUserPeriod(String ls, String startD, String endD) {
		return registerRepository.getUserPeriod(ls, startD, endD);
	}

	@Override
	public List<UserBean> getAllPeriod(String startD, String endD) {
		return registerRepository.getAllPeriod(startD, endD);
	}

}
