package com.alp.academy.main.service;


import java.util.List;

import org.springframework.data.jpa.repository.Query;

import com.alp.academy.main.model.UserBean;

public interface RegisterService {
	public void addData(UserBean user);
	
	public List<UserBean> getByLs(String ls, String mes);

	public List<UserBean> getRegisterByMonth(String mes);

	public List<UserBean> getUserPeriod(String ls, String startD, String endD);

	public List<UserBean> getAllPeriod(String startD, String endD);
	
}
