package com.alp.academy.main.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "registros")
public class UserBean {

	@Id
	private int user_id;
	private String name;
	private String entrada;
	private String salida;
	private String date;
	private String id;
	
	public UserBean() {}

	public UserBean(int user_id, String name, String entrada, String salida, String date, String id) {
		super();
		this.user_id = user_id;
		this.name = name;
		this.entrada = entrada;
		this.salida = salida;
		this.date = date;
		this.id = id;
	}

	public int getUserId() {
		return user_id;
	}

	public void setUserId(int user_id) {
		this.user_id = user_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEntrada() {
		return entrada;
	}

	public void setEntrada(String entrada) {
		this.entrada = entrada;
	}

	public String getSalida() {
		return salida;
	}

	public void setSalida(String salida) {
		this.salida = salida;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	@Override
	public String toString() {
		return "USER userId: " + user_id + " Id: " + " Name: " + name + " Entrada: " + entrada + " Salida: " + salida + " Date: " + date;
	}
}
