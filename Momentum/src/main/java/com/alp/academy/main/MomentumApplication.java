package com.alp.academy.main;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.alp.academy.main.model.InfoExcellBean;
import com.alp.academy.main.model.UserBean;


@EnableJpaRepositories("com.alp.academy.main.repositories")
@EntityScan("com.alp.academy.main.model")
@SpringBootApplication
public class MomentumApplication extends SpringBootServletInitializer {
   
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder app) {
      return app.sources(MomentumApplication.class);
    }
	
	public static void main(String[] args){
		SpringApplication.run(MomentumApplication.class, args);
		String rutaArchivo = "src\\main\\resources\\RegistrosMomentum.xls";
		List<InfoExcellBean> fullList = new ArrayList<>();
		try {
			Workbook workbook = WorkbookFactory.create(new File(rutaArchivo));
			Sheet sheet = workbook.getSheetAt(0);
			DataFormatter dataFormatter = new DataFormatter();
			sheet.forEach(row -> {
				String date = dataFormatter.formatCellValue(row.getCell(4));
				if (!date.equals("Date/Time")) {
					fullList.add(new InfoExcellBean(dataFormatter.formatCellValue(row.getCell(2)), 
							dataFormatter.formatCellValue(row.getCell(4)), 
							dataFormatter.formatCellValue(row.getCell(3))));
				}
			});
			workbook.close();
		} catch (InvalidFormatException | IOException e) {
			e.printStackTrace();
		}
		
		filterList(fullList);
	}

	public static List<UserBean> filterList(List<InfoExcellBean> fullList) {
		int i = 1, j = 1;
		List<InfoExcellBean> filteredList = new ArrayList<>();
		List<UserBean> users = new ArrayList<>();
		InfoExcellBean entrada = null, salida = null, siguiente = null;
		entrada = fullList.get(0);
		salida = fullList.get(0);
		while(i < fullList.size() - 1) {
			if (entrada.getDate().equals(fullList.get(i).getDate())) {
				if (entrada.getId().equals(fullList.get(i).getId())) {
					salida = fullList.get(i);
					siguiente = fullList.get(i + 1);
					
					if (!siguiente.getDate().equals(salida.getDate())) {
						users.add( new UserBean(j, entrada.getName(),
								entrada.getTime(), salida.getTime(),
								entrada.getDate(), entrada.getId()));
						filteredList.add(entrada);
						filteredList.add(salida);
						j++;
					}
				}
			} else {
				entrada = fullList.get(i);
				if (i == fullList.size() - 2) {
					siguiente = fullList.get( i + 1);
					if (entrada.getDate().equals(siguiente.getDate())) {
						users.add(new UserBean(j, entrada.getName(),
								entrada.getTime(), siguiente.getTime(),
								entrada.getDate(), entrada.getId()));
						filteredList.add(entrada);
						filteredList.add(siguiente);
						j++;
					}
				}
			}
			i++;
		}
		System.out.println(users.size());
		return users;
	}

}
