package com.alp.academy.main.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.alp.academy.main.model.UserBean;

public interface RegisterRepository extends JpaRepository<UserBean, Integer>{

	@Query("SELECT r FROM UserBean r WHERE r.name = ?1 AND r.date LIKE %?2% ")
	List<UserBean> getByLs(String ls, String mes);

	@Query("SELECT r FROM UserBean r WHERE r.date LIKE %?1% ")
	List<UserBean> getRegisterByMonth(String mes);

	@Query("SELECT r FROM UserBean r WHERE r.name = ?1 AND r.date BETWEEN ?2 AND ?3")
	List<UserBean> getUserPeriod(String ls, String startD, String endD);

	@Query("SELECT r FROM UserBean r WHERE r.date BETWEEN ?1 AND ?2")
	List<UserBean> getAllPeriod(String startD, String endD);

	
}
