package com.alp.academy.main.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.mapping.Array;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.alp.academy.main.model.UserBean;
import com.alp.academy.main.model.UserHours;
import com.alp.academy.main.model.UserPeriod;
import com.alp.academy.main.service.RegisterService;


@Controller
@RequestMapping(value = "/views")
public class ViewsController {
	
	@Autowired
	RegisterService registerService;
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String index(Model model) {
		return "index";
	}
	
	@RequestMapping(value = "/usersForm", method = RequestMethod.GET)
	public ModelAndView usersForm() {
		return new ModelAndView("searchUser", "command", new UserHours());
	}
	
	@RequestMapping(value = "/showHours", method = RequestMethod.POST)
	public String showHours(@ModelAttribute("SpringWeb")UserHours userHour, ModelMap model) throws ParseException {
		
		String ls = userHour.getLs();
		String mes = userHour.getMes();
		
		
		float horasTot = 0;
		Long diferencia;
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		Date horaE, horaS;
		List<UserBean> users = registerService.getByLs(ls, mes);
		System.out.println(users.size());
		
		if(users.isEmpty()) {
			return "errorPage";
		} else {
			for (UserBean user : users) {

				horaE = sdf.parse(user.getEntrada());
				horaS = sdf.parse(user.getSalida());
				diferencia = (horaS.getTime() - horaE.getTime());
				horasTot += diferencia.floatValue() / 3600000;	
			}
			
			userHour.setHoras(horasTot);
			
			model.addAttribute("ls", ls);
			model.addAttribute("mes", mes);
			model.addAttribute("horas", userHour.getHoras());
			model.addAttribute("command", new UserHours());
			
			return "monthlyHours";
		}
		
		
		
	}
	
	@RequestMapping(value = "/registersByMonth", method = RequestMethod.GET)
	public ModelAndView registersForm() {
		return new ModelAndView("registerByMonth", "command", new ArrayList<UserBean>());
	}
	
	@RequestMapping(value = "showRegisters", method = RequestMethod.POST)
	public String showRegisters(@ModelAttribute("SpringWeb")UserHours userHour, ModelMap model) {
		String mes = userHour.getMes();
		List<UserBean> registers = registerService.getRegisterByMonth(mes);
		
		if (registers.isEmpty()) {
			return "errorPage";
		}
		
		model.addAttribute("registers", registers);
		return "monthRegisters";
	}
	
	@RequestMapping(value = "/userPeriodForm", method = RequestMethod.GET)
	public ModelAndView periodForm() {
		return new ModelAndView("periodForm", "command", new ArrayList<UserBean>());
	}
	
	@RequestMapping(value = "/showPeriod", method = RequestMethod.POST)
	public String showPeriod(@ModelAttribute("SpringWeb")UserPeriod userPeriod, ModelMap model) {
		String ls = userPeriod.getLs();
		String startDate = userPeriod.getStartDate();
		String endDate = userPeriod.getEndDate();
		List<UserBean> registers = registerService.getUserPeriod(ls, startDate, endDate);
		
		if (registers.isEmpty()) {
			return "errorPage";
		}
		
		model.addAttribute("registers", registers);
		return "period";
	}
	
	@RequestMapping(value = "/allPeriodForm", method = RequestMethod.GET)
	public ModelAndView allForm() {
		return new ModelAndView("allForm", "command", new ArrayList<UserBean>());
	}
	
	@RequestMapping(value = "/showAllPeriod", method = RequestMethod.POST)
	public String showAll(@ModelAttribute("SpringWeb")UserPeriod userPeriod, ModelMap model) {
		String startDate = userPeriod.getStartDate();
		String endDate = userPeriod.getEndDate();
		List<UserBean> registers = registerService.getAllPeriod(startDate, endDate);
		
		if (registers.isEmpty()) {
			return "errorPage";
		}
		
		model.addAttribute("registers", registers);
		return "allPeriod";
	}
	
//	@GetMapping("/test")
//    public String hello(Model model, @RequestParam(value="name", required=false, defaultValue="World") String name) {
//        model.addAttribute("name", name);
//        return "searchUser";
//    }
}
