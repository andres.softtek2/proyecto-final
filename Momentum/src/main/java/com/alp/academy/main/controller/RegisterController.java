package com.alp.academy.main.controller;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.alp.academy.main.model.InfoExcellBean;
import com.alp.academy.main.model.UserBean;
import com.alp.academy.main.service.RegisterService;

@RestController
@RequestMapping("/api/v1/")
public class RegisterController {

	@Autowired
	RegisterService registerService;
	
	
	@RequestMapping(value = "users/{ls}/{mes}", method = RequestMethod.GET)
	public String getHoursByLs(@PathVariable String ls, @PathVariable String mes) throws ParseException {
		
		float horasTot = 0;
		Long diferencia;
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		Date horaE, horaS;
		List<UserBean> users = registerService.getByLs(ls, mes);
		System.out.println(users.size());
		
		if(users.isEmpty()) {
			return "ERROR AL PROCESAR PETICION, ASEGURATE QUE LOS DATOS SEAN CORRECTOS";
		} else {
			for (UserBean user : users) {

				horaE = sdf.parse(user.getEntrada());
				horaS = sdf.parse(user.getSalida());
				diferencia = (horaS.getTime() - horaE.getTime());
				horasTot += diferencia.floatValue() / 3600000;
				
			}
			return "Las horas trabajadas por: " + ls + " en el mes: " + mes + " fueron de: " + horasTot;
		}
		
	}
	
	@RequestMapping(value = "period/{mes}", method = RequestMethod.GET)
	public List<UserBean> getRegistersByMonth(@PathVariable String mes) {
		List<UserBean> registers = registerService.getRegisterByMonth(mes);
		if (registers.isEmpty()) {
			registers.add(new UserBean(0, "Busqueda", "Resultante", "Con", "Error", "Revisa los Datos"));
			return registers;
		}
		return registerService.getRegisterByMonth(mes);
	}
	
	@RequestMapping(value = "users", method = RequestMethod.POST)
	public List<UserBean> getUserPeriod(@RequestParam String ls, @RequestParam String startD, @RequestParam String endD){
		List<UserBean> registers = registerService.getUserPeriod(ls, startD, endD);
		if (registers.isEmpty()) {
			registers.add(new UserBean(0, "Busqueda", "Resultante", "Con", "Error", "Revisa los Datos"));
			return registers;
		}
		return registerService.getUserPeriod(ls, startD, endD);
	}
	
	@RequestMapping(value = "period", method = RequestMethod.POST)
	public List<UserBean> getAllPeriod(@RequestParam String startD, @RequestParam String endD){
		List<UserBean> registers = registerService.getAllPeriod(startD, endD);
		if (registers.isEmpty()) {
			registers.add(new UserBean(0, "Busqueda", "Resultante", "Con", "Error", "Revisa los Datos"));
			return registers;
		}
		return registerService.getAllPeriod(startD, endD);
	}
	
	
	@RequestMapping(value = "init", method = RequestMethod.GET)
	public void init() {
		String rutaArchivo = "src\\main\\resources\\RegistrosMomentum.xls";
		List<InfoExcellBean> list = new ArrayList<>();
		try {
			Workbook workbook = WorkbookFactory.create(new File(rutaArchivo));
			Sheet sheet = workbook.getSheetAt(0);
			DataFormatter dataFormatter = new DataFormatter();
			sheet.forEach(row -> {
				String date = dataFormatter.formatCellValue(row.getCell(4));
				if (!date.equals("Date/Time")) {
					list.add(new InfoExcellBean(dataFormatter.formatCellValue(row.getCell(2)), 
							dataFormatter.formatCellValue(row.getCell(4)), 
							dataFormatter.formatCellValue(row.getCell(3))));
				}
			});
			workbook.close();
		} catch (InvalidFormatException | IOException e) {
			e.printStackTrace();
		}
		
		int i = 1, j = 1;
		InfoExcellBean entrada = null, salida = null, siguiente = null;
		entrada = list.get(0);
		salida = list.get(0);
		while(i < list.size() - 1) {
			if (entrada.getDate().equals(list.get(i).getDate())) {
				if (entrada.getId().equals(list.get(i).getId())) {
					salida = list.get(i);
					siguiente = list.get(i + 1);
					
					if (!siguiente.getDate().equals(salida.getDate())) {
						registerService.addData( new UserBean(j, entrada.getName(),
								entrada.getTime(), salida.getTime(),
								entrada.getDate(), entrada.getId()));
						j++;
					}
				}
			} else {
				entrada = list.get(i);
				if (i == list.size() - 2) {
					siguiente = list.get( i + 1);
					if (entrada.getDate().equals(siguiente.getDate())) {
						registerService.addData(new UserBean(j, entrada.getName(),
								entrada.getTime(), siguiente.getTime(),
								entrada.getDate(), entrada.getId()));
						j++;
					}
				}
			}
			i++;
		}
	}
}
