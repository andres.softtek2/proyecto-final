<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<% String context = request.getContextPath(); %>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<title>Hours</title>
</head>
<body>
	
	<div class="container">
		<h1>El IS es: ${ls}</h1>
		<h1>El mes es: ${mes}</h1>
		<h1>Las horas trabajadas en el mes son: ${horas}</h1>
	</div>
	
	<div class="container">
	  <a href="<%=context %>/views/usersForm" class="list-group-item list-group-item-action">
	    Go Back
	  </a>
	</div>
</body>
</html>