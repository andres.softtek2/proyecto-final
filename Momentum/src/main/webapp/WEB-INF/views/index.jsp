<!DOCTYPE html>
<% String context = request.getContextPath(); %>
<html>
<head>
<meta charset="ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<title>Menu Principal</title>
</head>
<body>
	<div class="container">
	  <a href="<%=context %>/views/usersForm" class="list-group-item list-group-item-action">
	    Check Hours
	  </a>
	  <a href="<%=context %>/views/registersByMonth" class="list-group-item list-group-item-action">Get Registers By Month</a>
	  <a href="<%=context %>/views/userPeriodForm" class="list-group-item list-group-item-action">Get User's Period</a>
	  <a href="<%=context %>/views/allPeriodForm" class="list-group-item list-group-item-action">Get User's Period</a>
	</div>
</body>
</html>