package com.alp.academy.main;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.alp.academy.main.model.InfoExcellBean;
import com.alp.academy.main.model.UserBean;
import com.alp.academy.main.service.RegisterService;

@SpringBootTest
class MomentumApplicationTests {
	
	@Autowired
	RegisterService registerService;

	@Test
	void excellTest() throws EncryptedDocumentException, InvalidFormatException, IOException {
		String rutaArchivo = "src\\main\\resources\\RegistrosMomentum.xls";
		List<InfoExcellBean> fullList = new ArrayList<>();
		Workbook workbook = WorkbookFactory.create(new File(rutaArchivo));
		Sheet sheet = workbook.getSheetAt(0);
		DataFormatter dataFormatter = new DataFormatter();
		sheet.forEach(row -> {
			String date = dataFormatter.formatCellValue(row.getCell(4));
			if (!date.equals("Date/Time")) {
				fullList.add(new InfoExcellBean(dataFormatter.formatCellValue(row.getCell(2)), 
						dataFormatter.formatCellValue(row.getCell(4)), 
						dataFormatter.formatCellValue(row.getCell(3))));
			}
		});
		assertNotNull(workbook);
	}
	
	@Test
	void serviceTest() {
		List<UserBean> users = registerService.getRegisterByMonth("01");
		assertNotNull(users);
	}
	
	@Test
	void filterListTest() throws EncryptedDocumentException, InvalidFormatException, IOException {
		
		String rutaArchivo = "src\\main\\resources\\RegistrosMomentum.xls";
		List<InfoExcellBean> fullList = new ArrayList<>();
		Workbook workbook = WorkbookFactory.create(new File(rutaArchivo));
		Sheet sheet = workbook.getSheetAt(0);
		DataFormatter dataFormatter = new DataFormatter();
		sheet.forEach(row -> {
			String date = dataFormatter.formatCellValue(row.getCell(4));
			if (!date.equals("Date/Time")) {
				fullList.add(new InfoExcellBean(dataFormatter.formatCellValue(row.getCell(2)), 
						dataFormatter.formatCellValue(row.getCell(4)), 
						dataFormatter.formatCellValue(row.getCell(3))));
			}
		});
		
		assertNotNull(MomentumApplication.filterList(fullList));
	}

}
